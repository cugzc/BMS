package com.zc.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zc.domain.Vehicle;
import com.zc.domain.VehicleRulesDTO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SpringBootTest
@AutoConfigureMockMvc
public class testController{
    private static List<VehicleRulesDTO> list=new ArrayList<>();

    @Autowired
    private MockMvc mockMvc;
    private static int successNum;

    @BeforeAll
    public static void setUp() {
        successNum=0;
        Random random=new Random();
        for (int i = 0; i < 100; i++) {
            VehicleRulesDTO vehicleRulesDTO=new VehicleRulesDTO();
            vehicleRulesDTO.setFrameNum(random.nextInt(3)+1);//1-3
            vehicleRulesDTO.setRuleId(random.nextInt(2)+1);//1-2
            VehicleRulesDTO.Signal signal=new VehicleRulesDTO.Signal();
            signal.setMi((12-6)*random.nextDouble());//电压最小值0.0-6.0
            signal.setMx(6+(12-6)*random.nextDouble());//电压最大值6.0-12.0

            signal.setIi((12-6)*random.nextDouble());//电流最小值0.0-6.0
            signal.setIx(6+(12-6)*random.nextDouble());//电流最大值6.0-12.0
            vehicleRulesDTO.setSignal(signal);
            list.add(vehicleRulesDTO);
        }
    }

    @Test
    public void testSuccessRate() throws Exception {
        //String requestBody = "[{\"frameNum\": 1, \"ruleId\": 2, \"signal\": \"{\\\"Mx\\\":12.0,\\\"Mi\\\":0.6}\"}]";
        ObjectMapper objectMapper=new ObjectMapper();

        long startTime = System.currentTimeMillis();
        try {
            for (int i = 0; i < 100; i++) {
                String requestBody = objectMapper.writeValueAsString(list.get(i));
                requestBody="["+requestBody+"]";//数组符号要加上
                mockMvc.perform(post("/api/warn")
                                .content(requestBody)
                                .contentType("application/json"))
                        .andExpect(status().isOk());
                successNum++;
                //.andReturn();
            }
        }catch (Exception exception){
            exception.printStackTrace();
        }
        long endTime=System.currentTimeMillis();
        long responseTime = endTime - startTime;
        double successRate=successNum*1.0/100;
        System.out.println("预警成功率:"+successRate*100+"%");
        if(responseTime<100000){
            System.out.println("响应时间小于100s");
        }
    }
}

