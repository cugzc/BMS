package com.zc.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zc.VIDGenerator;
import com.zc.dao.VehicleDao;
import com.zc.domain.Vehicle;
import com.zc.domain.VehicleRulesDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class VehicleServiceTest {
    @Autowired
    private VehicleService vehicleService;
    @Autowired
    private VIDGenerator vidGenerator;
    @Test
    public void testGetAll(){
        List<Vehicle> list=vehicleService.getAll();
        System.out.println(list);
    }

    @Test
    public void testInsert(){
        String vid=vidGenerator.generateUniqueVID();
        Vehicle vehicle=new Vehicle();
        vehicle.setVid(vid);
        vehicle.setBatteryType("三元电池");
        vehicle.setMileage(200);
        vehicle.setHealthStatus(98);
        vehicleService.insert(vehicle);
    }
    @Test
    public void testGetResult(){
       // System.out.println(vehicleService.getResult(1,1,2,5));
    }
    @Test
    public void testJsonParse() throws JsonProcessingException {
        String json1="{\"Mx\":12.0,\"Mi\":0.6,\"ix\":1.2,\"ii\":0.2}";
        System.out.println(json1);
        ObjectMapper objectMapper=new ObjectMapper();
        VehicleRulesDTO.Signal signal = objectMapper.readValue(json1, VehicleRulesDTO.Signal.class);
        System.out.println(signal);

//        VehicleRulesDTO.Signal signal1=new VehicleRulesDTO.Signal();
//        signal1.setMx(20.0);
//        System.out.println(signal1);
//        ObjectMapper objectMapper=new ObjectMapper();
//        String json = objectMapper.writeValueAsString(signal1);
//        System.out.println(json);
//        VehicleRulesDTO.Signal signal2 = objectMapper.readValue(json, VehicleRulesDTO.Signal.class);
//        System.out.println(signal2);
    }
}
