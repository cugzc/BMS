package com.zc.others;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zc.domain.VehicleRulesDTO;

import java.io.IOException;

//未使用
public class SignalDeserializer extends JsonDeserializer<VehicleRulesDTO.Signal> {

    @Override
    public VehicleRulesDTO.Signal deserialize(JsonParser p, DeserializationContext deserializationContext)
            throws IOException {
        ObjectMapper objectMapper = (ObjectMapper) p.getCodec();
        String signalJson = p.getText();
        return objectMapper.readValue(signalJson, VehicleRulesDTO.Signal.class);
    }
}
