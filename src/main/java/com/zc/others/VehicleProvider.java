package com.zc.others;
import org.apache.ibatis.jdbc.SQL;
import com.zc.domain.VehicleRulesDTO;

import java.lang.reflect.Field;

public class VehicleProvider {
    public String getResult1(VehicleRulesDTO dto) {
        return new SQL() {{
            //SELECT("vehicle.frameNum, vehicle.batteryType, rules.warningName, rules.level");
            SELECT("vehicle.frameNum, vehicle.batteryType, rules.warningName, rules.level," +
                    "case when rules.level=-1 then '没有报警' else '发生了报警' end as remark");

            FROM("vehicle, rules");
            WHERE("vehicle.batteryType = rules.batteryType");
            WHERE("vehicle.frameNum = #{frameNum}");
            if(dto.getRuleId()==-1){//什么规则也没指定
                AND();//该方法会加一层大括号
                WHERE("rules.ruleId =1 "
                        + "and "
                        +"(#{signal.Mx} - #{signal.Mi}) >= rules.minDifference and "
                        + "(rules.maxDifference IS NULL OR (#{signal.Mx} - #{signal.Mi}) < rules.maxDifference)"
                        +" or "
                        + "rules.ruleId =2 "
                        + "and "
                        +"(#{signal.Ix} - #{signal.Ii}) >= rules.minDifference and "
                        + "(rules.maxDifference IS NULL OR (#{signal.Ix} - #{signal.Ii}) < rules.maxDifference)");
                //WHERE();
            }
            else {
                WHERE("rules.ruleId = #{ruleId}");//指定了某一规则
                if (dto.getSignal().getMx()!=-1 &&dto.getSignal().getMi()!=-1 ) {//传入Mx与Mi
                    WHERE("(#{signal.Mx} - #{signal.Mi}) >= rules.minDifference");
                    WHERE("(rules.maxDifference IS NULL OR (#{signal.Mx} - #{signal.Mi}) < rules.maxDifference)");
                    }
                else if(dto.getSignal().getIx()!=-1 &&dto.getSignal().getIi()!=-1 ){//传入Ix与Ii
                    WHERE("(#{signal.Ix} - #{signal.Ii}) >= rules.minDifference");
                    WHERE("(rules.maxDifference IS NULL OR (#{signal.Ix} - #{signal.Ii}) < rules.maxDifference)");
                }
            }
        }}.toString();
        //第一对大括号是声明这是个匿名内部类；第二对大括号是个普通代码块，在这个代码块中进行了一些操作，影响了this对象，使得其产生对应的sql语句
    }
}
