package com.zc.config;


import com.zc.VIDGenerator;
import org.springframework.context.annotation.Bean;

public class VidGeneratorConfig {
    @Bean
    public VIDGenerator vidGenerator(){
        VIDGenerator vidGenerator=new VIDGenerator(VIDGenerator.allCharSet,16);
        return vidGenerator;
    }
}
