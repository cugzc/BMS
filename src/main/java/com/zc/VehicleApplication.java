package com.zc;

import com.zc.config.RedisConfig;
import com.zc.config.VidGeneratorConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;


@SpringBootApplication
@Import({VidGeneratorConfig.class, RedisConfig.class})
public class VehicleApplication {
    public static void main(String[] args) {
        SpringApplication.run(VehicleApplication.class,args);
    }
}
