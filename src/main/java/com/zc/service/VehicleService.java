package com.zc.service;

import com.zc.domain.Vehicle;
import com.zc.domain.VehicleRulesDTO;
import com.zc.domain.VehicleRulesVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface VehicleService {

    /**
     * 插入
     * @param vehicle
     * @return
     */
    public boolean insert(Vehicle vehicle);

    /**
     * 修改
     * @param vehicle
     * @return
     */
    public boolean update(Vehicle vehicle);

    /**
     * 按id删除
     * @param frameNum
     * @return
     */
    public boolean delete(Integer frameNum);

    /**
     * 按id查询
     * @param frameNum
     * @return
     */
    public Vehicle getById(Integer frameNum);

    /**
     * 查询全部
     * @return
     */
    public List<Vehicle> getAll();
    public List<Vehicle> getPaged(int currentPageNum,int currentPageSize);
    public List<VehicleRulesVO> getResults(VehicleRulesDTO vehicleRulesDTO);


}
