package com.zc.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zc.dao.RedisDao;
import com.zc.dao.VehicleDao;
import com.zc.domain.Vehicle;
import com.zc.domain.VehicleRulesDTO;
import com.zc.domain.VehicleRulesVO;
import com.zc.service.VehicleService;
import io.lettuce.core.RedisConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

@Service
public class VehicleServiceImpl implements VehicleService {
    @Autowired
    private VehicleDao vehicleDao;
    @Autowired
    private RedisDao redisDao;
    @Override
    public boolean insert(Vehicle vehicle) {
        return vehicleDao.insert(vehicle)>0;
    }

    @Override
    public boolean delete(Integer frameNum) {
        return vehicleDao.deleteById(frameNum)>0;
    }

    @Override
    public boolean update(Vehicle vehicle) {
        return vehicleDao.updateById(vehicle)>0;
    }

    @Override
    public Vehicle getById(Integer frameNum) {
        return vehicleDao.selectById(frameNum);
    }

    @Override
    public List<Vehicle> getAll() {
        return vehicleDao.selectList(null);
    }

    @Override
    public List<Vehicle> getPaged(int currentPageNum, int currentPageSize) {
        IPage<Vehicle> page1=new Page(currentPageNum,currentPageSize);
        IPage<Vehicle> page2 = vehicleDao.selectPage(page1, null);//page1与page2是同一个对象
//        System.out.println("当前页码值："+page1.getCurrent());
//        System.out.println("每页显示数："+page1.getSize());
//        System.out.println("一共多少页："+page1.getPages());
//        System.out.println("一共多少条数据:"+page1.getTotal());
        return page2.getRecords();
    }

    @Override
    public List<VehicleRulesVO> getResults(VehicleRulesDTO vehicleRulesDTO) {
        try {
            String key=vehicleRulesDTO.toString();
            List<VehicleRulesVO> list=(List<VehicleRulesVO>)redisDao.get(key);
            System.out.println("redis查询："+list);
            //缓存中没有
            if(list==null){
                //去数据库找
                list=vehicleDao.getResults(vehicleRulesDTO);
                //数据库有
                if(list!=null){
                    redisDao.setex(key,list,1, TimeUnit.MINUTES);
                    return list;
                    //数据库也没有
                }else {
                    return null;
                }
            }
            //缓存中有
            else
            {
                System.out.println("缓存中的list："+list);
                return list;
            }
        }catch (RedisConnectionFailureException redisConnectionFailureException){
            //System.out.println("------------------由于redis连接异常，使用数据库完成了检索-------------------");
            Logger logger=Logger.getLogger("logger");
            logger.info("----------------------------------------由于redis连接异常，使用数据库完成了检索-------------------------------------");
            return vehicleDao.getResults(vehicleRulesDTO);
        }
    }
}
