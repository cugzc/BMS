package com.zc;

import java.security.SecureRandom;
import java.util.HashSet;
import java.util.Set;

public class VIDGenerator {
    public static  String allCharSet = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789";
    private String charSet;
    private int vidLength;
    private SecureRandom random = new SecureRandom();
    private Set<String> VIDs= new HashSet<>();

    public VIDGenerator(String charSet,int length)
    {
        this.charSet=charSet;
        this.vidLength=length;
    }
    public String generateUniqueVID() {
        String vid;
        do {
            vid = generateRandomVID();
        } while (VIDs.contains(vid));  // 检查唯一性
        VIDs.add(vid);
        return vid;
    }

    private String generateRandomVID() {
        StringBuffer vid = new StringBuffer();
        for (int i = 0; i < this.vidLength; i++) {
            //RANDOM.nextInt(x):  生成一个介于0（包括）和指定值x（不包括）之间的随机整数
            vid.append(charSet.charAt(random.nextInt(charSet.length())));
        }
        return vid.toString();
    }

    public static void main(String[] args) {
        VIDGenerator vidGenerator=new VIDGenerator(VIDGenerator.allCharSet,16);
        for (int i = 0; i < 10; i++) {
            String vid = vidGenerator.generateUniqueVID();
            System.out.println(vid);
            System.out.println(vidGenerator.VIDs);
        }
    }
}