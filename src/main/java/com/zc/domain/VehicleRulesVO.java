package com.zc.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VehicleRulesVO {
    //@JsonProperty("车架编号")
    private int frameNum;
    //@JsonProperty("电池类型")
    private String batteryType;
    private String warningName;
    int level;
    @JsonProperty("备注")
    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getFrameNum() {
        return frameNum;
    }

    public void setFrameNum(int frameNum) {
        this.frameNum = frameNum;
    }

    public String getBatteryType() {
        return batteryType;
    }

    public void setBatteryType(String batteryType) {
        this.batteryType = batteryType;
    }

    public String getWarningName() {
        return warningName;
    }

    public void setWarningName(String warningName) {
        this.warningName = warningName;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "VehicleRulesVO{" +
                "frameNum=" + frameNum +
                ", batteryType='" + batteryType + '\'' +
                ", warningName='" + warningName + '\'' +
                ", level=" + level +
                ", remark='" + remark + '\'' +
                '}';
    }
}
