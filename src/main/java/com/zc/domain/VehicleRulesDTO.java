package com.zc.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.apache.ibatis.annotations.Param;

public class VehicleRulesDTO {
    private int frameNum;
    private int ruleId=-1;//默认-1代表没传入规则
//    private double min;
//    private double max;
   // @JsonDeserialize(using = SignalDeserializer.class)
    private Signal signal;

    public int getFrameNum() {
        return frameNum;
    }

    public void setFrameNum(int frameNum) {
        this.frameNum = frameNum;
    }

    public int getRuleId() {
        return ruleId;
    }

    public void setRuleId(int ruleId) {
        this.ruleId = ruleId;
    }

//    public double getMin() {
//        return min;
//    }
//
//    public void setMin(double min) {
//        this.min = min;
//    }
//
//    public double getMax() {
//        return max;
//    }
//
//    public void setMax(double max) {
//        this.max = max;
//    }

    public Signal getSignal() {
        return signal;
    }
    //@JsonDeserialize(using = SignalDeserializer.class)
    public void setSignal(Signal signal) {
        this.signal = signal;
    }

//    @Override
//    public String toString() {
//        return "VehicleRulesDTO{" +
//                "frameNum=" + frameNum +
//                ", ruleId=" + ruleId +
//                ", min=" + min +
//                ", max=" + max +
//                '}';
//    }

    @Override
    public String toString() {
        return "VehicleRulesDTO{" +
                "frameNum=" + frameNum +
                ", ruleId=" + ruleId +
                ", signal=" + signal +
                '}';
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Signal{

        @JsonProperty("Mx")
        //有坑，下面这行注释也没必要深究
        // 没有上面的注解，则将json字符串中的属性"mx"（解析器认为）解析出来赋值；加入上面注解，可额外解析字符串中的"Mx"，之前的"mx"依然可被解析为Mx变量。
        private double Mx=-1;//最高电压
        @JsonProperty("Mi")
        private double Mi=-1;//最低电压
        @JsonProperty("Ix")
        private double Ix=-1;//最高电流
        @JsonProperty("Ii")
        private double Ii=-1;//最低电流
        public Signal(){};
        public double getMx() {
            return Mx;
        }
        public Signal(String json) throws JsonProcessingException {
            ObjectMapper objectMapper=new ObjectMapper();
            Signal signal = objectMapper.readValue(json, Signal.class);
            //this.Mx=signal.getMx();
            this.Mx=signal.Mx;//自身类内可访问私有变量
            this.Mi=signal.getMi();
            this.Ix=signal.getIx();
            this.Ii=signal.getIi();
        }
        public void setMx(double mx) {
            Mx = mx;
        }

        public double getMi() {
            return Mi;
        }

        public void setMi(double mi) {
            Mi = mi;
        }

        public double getIx() {
            return Ix;
        }

        public void setIx(double ix) {
            Ix = ix;
        }

        public double getIi() {
            return Ii;
        }

        public void setIi(double ii) {
            Ii = ii;
        }

        @Override
        public String toString() {
            return "Signal{" +
                    "Mx=" + Mx +
                    ", Mi=" + Mi +
                    ", Ix=" + Ix +
                    ", Ii=" + Ii +
                    '}';
        }
    }
}
