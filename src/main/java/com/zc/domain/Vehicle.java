package com.zc.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@TableName("vehicle")
public class Vehicle {

    @TableField("vid")
    private String vid;

    @TableId("frameNum")
    private int frameNum;

    @TableField("batteryType")
    private String batteryType;

    @TableField("mileage")
    private int mileage;

    @TableField("healthStatus")
    private int healthStatus;

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public int getFrameNum() {
        return frameNum;
    }

    public void setFrameNum(int frameNum) {
        this.frameNum = frameNum;
    }

    public String getBatteryType() {
        return batteryType;
    }

    public void setBatteryType(String batteryType) {
        this.batteryType = batteryType;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public int getHealthStatus() {
        return healthStatus;
    }

    public void setHealthStatus(int healthStatus) {
        this.healthStatus = healthStatus;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "vid='" + vid + '\'' +
                ", frameNum=" + frameNum +
                ", batteryType='" + batteryType + '\'' +
                ", mileage=" + mileage +
                ", healthStatus=" + healthStatus +
                '}';
    }
}
