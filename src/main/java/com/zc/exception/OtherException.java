package com.zc.exception;

public class OtherException extends RuntimeException{
    private  Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public OtherException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public OtherException(Integer code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }
}
