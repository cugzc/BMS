package com.zc.dao;

import java.util.concurrent.TimeUnit;

public interface RedisDao {
    public void set(String key,Object value);

    public void setex(String key, Object value, long time, TimeUnit unit);

    public Object get(String key);
    public void deleted(String key);
}
