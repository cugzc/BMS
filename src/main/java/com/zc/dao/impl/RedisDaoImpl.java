package com.zc.dao.impl;

import com.zc.dao.RedisDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.concurrent.TimeUnit;

@Repository
public class RedisDaoImpl implements RedisDao {
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    @Override
    public void set(String key, Object value) {
        redisTemplate.opsForValue().set(key,value);
    }

    @Override
    public void setex(String key, Object value, long time, TimeUnit unit) {
        redisTemplate.opsForValue().set(key,value,time,unit);
    }

    @Override
    public Object get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    @Override
    public void deleted(String key) {
        redisTemplate.delete(key);
    }
}
