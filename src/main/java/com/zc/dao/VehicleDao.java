package com.zc.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zc.domain.Vehicle;
import com.zc.domain.VehicleRulesDTO;
import com.zc.domain.VehicleRulesVO;
import com.zc.others.VehicleProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

@Mapper
public interface VehicleDao extends BaseMapper<Vehicle> {

//    @Select("SELECT vehicle.frameNum,vehicle.batteryType,rules.warningName,rules.`level` from vehicle,rules WHERE "+
//            "vehicle.batteryType=rules.batteryType "+
//            "and vehicle.frameNum=#{frameNum} "+
//            "and rules.ruleId=#{ruleId} "+
//            "and (#{max}-#{min}) >=rules.minDifference and (rules.maxDifference is null or (#{max}-#{min})<rules.maxDifference)")
//    public VehicleRulesVO getResult(VehicleRulesDTO vehicleRulesDTO);

    @SelectProvider(type = VehicleProvider.class,method = "getResult1")
    public List<VehicleRulesVO> getResults(VehicleRulesDTO vehicleRulesDTO);
    //public VehicleRulesVO getResult(@Param("frameNum") int frameNum, @Param("ruleId") int ruleId, @Param("min")double min, @Param("max")double max);
}
