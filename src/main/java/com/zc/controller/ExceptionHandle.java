package com.zc.controller;

import com.zc.exception.OtherException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandle {
    @ExceptionHandler(OtherException.class)
    public Result handleException1(OtherException otherException){
        return new Result(otherException.getCode(),null, otherException.getMessage());
    }
}
