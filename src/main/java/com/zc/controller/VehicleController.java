package com.zc.controller;

import com.zc.domain.VehicleRulesDTO;
import com.zc.domain.VehicleRulesVO;
import com.zc.exception.OtherException;
import com.zc.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/warn")
public class VehicleController {
    @Autowired
    private VehicleService vehicleService;
//    @PostMapping()
//    public List<VehicleRulesVO> getResults1(@RequestBody VehicleRulesDTO vehicleRulesDTO){
//        System.out.println("-------------");
//        System.out.println(vehicleRulesDTO.getSignal());
//        System.out.println("-------------");
//        return vehicleService.getResults(vehicleRulesDTO);
//    }

    @PostMapping()
    public Result getResults(@RequestBody List<VehicleRulesDTO> vehicleRulesDTOList){
        try {
            List<VehicleRulesVO> list=new ArrayList<>();
            for(VehicleRulesDTO vehicleRulesDTO:vehicleRulesDTOList){
                //throw new PathException(Code.GET_FAILURE,"获取失败了");
                list.addAll(vehicleService.getResults(vehicleRulesDTO));
            }
            Integer code = list!=null ? Code.GET_OK : Code.GET_FAILURE;
            String msg=list!=null ? "ok": "查询失败";
            return new Result(code,list,msg);
        } catch (Exception exception){
            //exception.printStackTrace();
            throw new OtherException(Code.GET_FAILURE,"出错了，原因如下："+exception.getMessage());
        }
    }
}
