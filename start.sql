-- 创建数据库bms
CREATE DATABASE IF NOT EXISTS bms;
use bms;

-- 创建表vehicle
CREATE TABLE IF NOT EXISTS vehicle(
vid VARCHAR(20),
frameNum int PRIMARY KEY auto_increment,
batteryType VARCHAR(20),
mileage int,
healthStatus int);

INSERT INTO vehicle VALUES('4F6Uz5qWe6tSTqwT',1,'三元电池',100,100),('hjzUgaSBdV8t7UE3',2,'铁锂电池',600,95),('wGdC5QkcQJvqXR7t',3,'三元电池',300,98);

-- 创建表rules
CREATE TABLE IF NOT EXISTS rules(
id int,
ruleId int,
warningName VARCHAR(20),
batteryType VARCHAR(20),
minDifference DOUBLE,
maxDifference DOUBLE,
level int);

INSERT INTO rules VALUES(1,1,'电压差报警','三元电池',5,null,0);
INSERT INTO rules VALUES(1,1,'电压差报警','三元电池',3,5,1);
INSERT INTO rules VALUES(1,1,'电压差报警','三元电池',1,3,2);
INSERT INTO rules VALUES(1,1,'电压差报警','三元电池',0.6,1,3);
INSERT INTO rules VALUES(1,1,'电压差报警','三元电池',0.2,0.6,4);
INSERT INTO rules VALUES(1,1,'电压差报警','三元电池',0,0.2,-1);

INSERT INTO rules VALUES(2,1,'电压差报警','铁锂电池',2,null,0);
INSERT INTO rules VALUES(2,1,'电压差报警','铁锂电池',1,2,1);
INSERT INTO rules VALUES(2,1,'电压差报警','铁锂电池',0.7,1,2);
INSERT INTO rules VALUES(2,1,'电压差报警','铁锂电池',0.4,0.7,3);
INSERT INTO rules VALUES(2,1,'电压差报警','铁锂电池',0.2,0.4,4);
INSERT INTO rules VALUES(2,1,'电压差报警','铁锂电池',0,0.2,-1);


INSERT INTO rules VALUES(3,2,'电流差报警','三元电池',3,null,0);
INSERT INTO rules VALUES(3,2,'电流差报警','三元电池',1,3,1);
INSERT INTO rules VALUES(3,2,'电流差报警','三元电池',0.2,1,2);
INSERT INTO rules VALUES(3,2,'电流差报警','三元电池',0,0.2,-1);

INSERT INTO rules VALUES(4,2,'电流差报警','铁锂电池',1,null,0);
INSERT INTO rules VALUES(4,2,'电流差报警','铁锂电池',0.5,1,1);
INSERT INTO rules VALUES(4,2,'电流差报警','铁锂电池',0.2,0.5,2);
INSERT INTO rules VALUES(4,2,'电流差报警','铁锂电池',0,0.2,-1);

-- 对表rules的minDifference与maxDifference列添加索引
CREATE INDEX index_minDifference on rules (minDifference);
CREATE INDEX index_maxDifference on rules (maxDifference);
-- 对表rules添加一个联合索引
CREATE INDEX index_ruleId_minDifference on rules (ruleId,minDifference);


